## Overview

This is a telephone number decoder application that contains a dictionary and returns matches for telephone numbers from the words in the dictionary.
The alphabet mapping is akin to that of T9 style keyboard typically found on legacy mobile phones. 
The dictionary itself can be a custom dictionary or the default one. A custom dictionary can be set via the command line the using -d option.

## Installation


#### Prerequisites


- Ruby 2.x.x
- Bundler gem (can be installed using gem install bundler)

You can git clone this repository onto your machine and 'bundle install' to have all the dependancies needed. 


#### Dependancies

RubyGems used:-

- RSpec 3.0

This application has been successfully run and tested on Ruby 2.0.0p481 with RSpec 3.0.0

## Running this application

This application can be run by executing the following command from the application root

ruby src/tele_decoder.rb {input_files} -d {custom_dictionary}

Example:

ruby src/tele_decoder.rb 'test_data/test.in' -d 'test_data/my_dictionary.in'

All arguments are optional. 

In absence of the -d argument, a default dictionary is used (test_data/dictionary.in) and in absence of input files STDIN is used as input. 
In case of STDIN the output is printed to STDOUT. In case of input files, a corresponding output file with .out extension is created at the same path. 
