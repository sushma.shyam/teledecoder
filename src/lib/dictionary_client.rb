# The dictionary client creates and interfaces with the dictionary. It dependancy injects the string encoder into the dictionary

require_relative 'dictionary'
require_relative 'string_encoder'

class DictionaryClient
  def initialize
    string_encoder = StringEncoder.new
    @dictionary = Dictionary.new(string_encoder)   
  end
  
  def build_dictionary(file)
    File.open(file, "r").each_line do |line|
      @dictionary.add(line.chomp) unless line.chomp.empty?
    end   
  end
  
  def decode_number(number)
    @dictionary.best_matches(number.chomp.gsub(/[^[\w.]]/, ''))
  end
  
end
