# The dictionary store class is the data structure that holds the dictionary in memory. 
# It is implemented as a Trie with each node containing a character of the key/code 
# and the terminating nodes containing the values 

class DictionaryStore

  def initialize
    @root = Node.new(nil)
  end

  def add_word(code, word)
    code = code.to_s
    return nil if code.nil?
    add(code, word)
  end

  def word_for_code(code)
    node = @root

    code.each_char do |char|
      node = node.find(char.to_s, true, [])
      break if node.nil?
    end

    node.words.first if node && node.words
  end

  def suggest_word_for_code(code)
    StoreScan.new(@root, code).word
  end

  private

  def add(code, word)
    node = @root

    code.each_char do |char|
      char_node = node.find(char,true,[])

      if char_node.nil?
        char_node = Node.new(char)
        node.add_references(char,char_node)
      end
  
      node = char_node
    end
    node.add_word(word)
  end
  
  # A store scan object is build for each scan with a code
  class StoreScan

    def initialize(dictionary, code)
      @node_array = []
      @node_array.push(dictionary)
      @level = 0
      @tried_nodes = []
      @exact = false
      @word_found = ''
      @code = code
      @suggestions = []
    end
    
    def word
      until @node_array.empty? do
        node = next_node       
  
        if node.nil? || @code.length == @level          
          retract                
        else
          if match?(node)                             
            update_suggestions(node.words)
  
            if @word_found.index('*') == nil # perfect
              return node.words
            end
  
          end
        end
  
      end    
      @suggestions   
    end
    
  private
    def next_node
      @node_array.first.find(@code[@level], exact,@tried_nodes[@level])  
    end
    
    
    def match?(node)           
      @node_array.insert(0,node)
      char_at_node = node.data
      (char_at_node == @code[@level])? @word_found[@level] = char_at_node : @word_found[@level] = '*'
      @tried_nodes[@level] ||= []
      @tried_nodes[@level].push(char_at_node)
      @level += 1
      return (@level == @code.length) && node.words
    end
    
    def exact
      return false if @level == 0
      (@code[@level-1] != @word_found[@level-1]) #check if previous characters matched
    end
    
    def update_suggestions(words)
      suggestions = replace_differences(words)    
  
      suggestions.each do |suggestion|
        @suggestions.push(suggestion) unless @suggestions.include?(suggestion)
      end
  
    end
    
    def replace_differences(words)
      cursor = 0
      suggestions = words.map { |element| element.dup } # ensure that a deep copy is done, so that the original dictionary is not modified
  
      while cursor < @code.length do 
        wildcard = @word_found.index('*',cursor)      
  
        break if wildcard.nil?
  
        suggestions.each do |match|
          match[wildcard] = @code[wildcard]
        end 
  
        cursor = wildcard + 1
      end       
      suggestions
    end   
    
    def retract
      @node_array.shift
      @tried_nodes[@level] = [] 
      @level -= 1
    end
  end
  # Represents a node in the Trie
  class Node
    attr_reader :data
    attr_reader :words
    
    def initialize(data)
      @data = data
      @references = Hash.new
      @words = []
    end

    def references_except(expect)
      if expect.nil?
        @references
      else
        @references.select{|element| !expect.include?(element)}
      end
    end

    def find(key,exact,except)
      reduced_references = references_except(except)
      return nil if reduced_references.nil? || reduced_references.empty?
      exact_match = reduced_references[key]
      
      if exact
        exact_match
      else
        exact_match ||= reduced_references[reduced_references.keys.sample]
      end
    end

    def add_word(word)
      @words.push(word) unless @words.include?(word)
    end

    def add_references(key,value)
      @references[key] = value
    end
    
    def no_words?
      @words.empty?
    end
  end
end
