class StringEncoder    
  def encode(string)
    code = string.downcase.tr('a-z-', '22233344455566677778889999.')
    code.gsub!(/[^[\w.]]/, '')
    code
  end  
end

