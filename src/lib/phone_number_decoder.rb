# This is the application class that handles arguments and delegates the dictionary building and decoding to the dictionary client 

require_relative 'dictionary_client'

class PhoneNumberDecoder
  DEFAULT_DICTIONARY = 'test_data/dictionary.in'
  def initialize
    @client = DictionaryClient.new
    @dictionary_file = DEFAULT_DICTIONARY
    @input_files = [$stdin]
  end
  
  def run_decoder
    parse_input_args
    build_dictionary
    decode
  end
private  
  def parse_input_args
    return if ARGV.empty?
    d_index = ARGV.index('-d')
    
    if d_index && d_index + 1 < ARGV.count 
      @dictionary_file = ARGV[d_index + 1] 
      ARGV.delete_at(d_index+1)
    end
    
    ARGV.delete_at(d_index) if d_index
    
    unless ARGV.empty?
      @input_files = ARGV       
    end
  end
  
  def build_dictionary
    @client.build_dictionary(@dictionary_file)  
  end
  
  def output_file(input_file)
    return $stdout if input_file == $stdin
    input_file + '.out'
  end
  
  def print_user_prompts        
      puts "Enter EOF character (usually Ctrl+D on MAC/Linux) to exit"
      puts "Enter numbers to be decoded"    
  end
  
  def decode_file(input_file)
    print_user_prompts if input_file == $stdin
    output_file = output_file(input_file)
    
    unless input_file == $stdin
      input_file = File.open(input_file,'r') 
      output_file = File.open(output_file,'w')
    end
    
    until input_file.eof?            
      output_file.puts @client.decode_number(input_file.gets)
    end
    
  ensure
    unless input_file == $stdin
      output_file.close
      input_file.close
    end    
  end  
  
  def decode    
    @input_files.each do |input_file|
      decode_file(input_file) if File.exist?(input_file) 
    end           
  end    
end
