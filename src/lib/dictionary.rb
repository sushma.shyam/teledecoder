# The dictionary stores and retrieves words from its store. It uses the injected encoder to create keys for storing the words 
require_relative 'dictionary_store'

class Dictionary
  def initialize(string_encoder)
    @string_encoder = string_encoder 
    @store = DictionaryStore.new
  end
  
  def add(string)
    code = @string_encoder.encode(string)
    @store.add_word(code, string)
    code
  end
  
  def best_matches(code)
    @store.suggest_word_for_code(code)
  end
end
