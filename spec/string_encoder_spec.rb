require_relative 'spec_helper'
require_relative '../src/lib/string_encoder'

describe "StringEncoder" do 
  it "encodes all characters correctly" do
    expect(StringEncoder.new.encode("ABCDEFGHIJKLMNOPQRSTUVWXYZ-")).to eq("22233344455566677778889999.")
  end
  it "ignores other punctuations" do
    expect(StringEncoder.new.encode("ABC:DEFGHIJKLMNOPQRSTUVWXYZ-")).to eq("22233344455566677778889999.")
  end
end
