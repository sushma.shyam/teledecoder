require_relative 'spec_helper'
require_relative '../src/lib/dictionary_store'

describe "DictionaryStore" do
  context "when empty" do
    let(:dictionary){DictionaryStore.new}
    it "should return nil word for any code" do
      expect(dictionary.word_for_code('123522')).to eq(nil)
    end
    it "should return nil suggested words for any code" do
      expect(dictionary.suggest_word_for_code('123522')).to eq([])
    end        
  end
  context "when not empty" do
    before(:all) do
      @ds = DictionaryStore.new
      @ds.add_word("2255.63", "CALL-ME")
      @ds.add_word("3569377", "FLOWERS")
      @ds.add_word("2665.2.222", "BOOK-A-CAB")
      @ds.add_word("23678.2.738", "ADOPT-A-PET")
      @ds.add_word("68742", "MUSIC")
      @ds.add_word("2255", "BALL")
      @ds.add_word("2255", "BALK")
      @ds.add_word("2855", "BULK")
    end
    it "should return exact match if available" do
      expect(@ds.suggest_word_for_code("3569377")).to eq(["FLOWERS"])
    end
    it "should return all exact matches if available" do
      expect(@ds.suggest_word_for_code("2255")).to eq(["BALL","BALK"])
    end
    it "should return a suggestion if available" do
      expect(@ds.suggest_word_for_code("3569378")).to eq(["FLOWER8"])
    end
    it "should return all suggestions if available" do
      expect(@ds.suggest_word_for_code("2253")).to eq(["BAL3","B2L3"])
    end
    it "should not return longer words" do
      expect(@ds.suggest_word_for_code("2255.6")).to eq([])
    end
    it "should not return a match if 2 consequtive characters mismatch" do
      expect(@ds.suggest_word_for_code("68739")).to eq([])
    end
  end
end
