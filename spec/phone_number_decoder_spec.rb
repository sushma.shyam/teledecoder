require 'fileutils'
require_relative 'spec_helper'
require_relative '../src/lib/phone_number_decoder'

describe 'PhoneNumberDecoder' do
  context 'no dictionary file passed and input file' do
    before(:all) do
      @decoder = PhoneNumberDecoder.new
      ARGV.replace ['test_data/t1.in']
      @decoder.run_decoder 
    end
    it 'produces corresponding output file' do
      expect(File.exist?('test_data/t1.in.out')).to eq(true)  
    end
    it 'uses default dictionary and decodes contents in the input file and writes decoded matches into output file' do     
      expect(FileUtils.compare_file('test_data/t1.in.out', 'test_data/t1.expected')).to eq(true)
    end
  end
  context 'a custom dictionary file and input file are passed' do
    before(:all) do
      @decoder = PhoneNumberDecoder.new
      ARGV.replace ['test_data/t2.in', '-d', 'test_data/my_dictionary.in']
      @decoder.run_decoder 
    end
    it 'produces corresponding output file' do
      expect(File.exist?('test_data/t2.in.out')).to eq(true)  
    end
    it 'uses custom dictionary and decodes contents in the input file and writes decoded matches into output file' do     
      expect(FileUtils.compare_file('test_data/t2.in.out', 'test_data/t2.expected')).to eq(true)
    end
  end

end
