require_relative 'spec_helper'
require_relative '../src/lib/dictionary'

describe "Dictionary" do
  let(:string_encoder){StringEncoder.new}
  let(:dictionary){Dictionary.new(string_encoder)}
  it "adds a word to the dictionary" do
    expect(dictionary.add("CALL-ME")).to eq(string_encoder.encode("CALL-ME"))
  end
  it "no matches returned for unavailable word" do
    expect(dictionary.best_matches("1234")).to eq([])
  end  
  it "returns matches for added word" do
    dictionary.add("CALL-ME")
    expect(dictionary.best_matches(string_encoder.encode("CALL-ME"))).to eq(["CALL-ME"])
  end    
end
